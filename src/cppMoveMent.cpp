
#include <Rcpp.h>
using namespace Rcpp;

static const int red = -1;
static const int white = 0;
static const int blue = 1;
using std::vector;

// [[Rcpp::export(.moveRedCpp)]]
List moveRedCpp(List Grid) {
  SEXP Matrix = Grid["Matrix"];
  Rcpp::NumericMatrix CppMatrix = as<Rcpp::NumericMatrix>(Matrix);
  unsigned long curnum = 0;
  int n = CppMatrix.ncol();
  for (int i = 0; i < CppMatrix.nrow(); i++) {
    int start = 0;
    int end = n - 1;
    if (CppMatrix(i, start) == white && CppMatrix(i, end) == red) {
      CppMatrix(i, end) = white;
      CppMatrix(i, start) = red;
      start++;
      end--;
    }
    for (int j = start; j < end; j++) {
      if (CppMatrix(i, j) == red && CppMatrix(i, j + 1) == white) {
        CppMatrix(i, j) = white;
        CppMatrix(i, j + 1) = red;
        j++;
        curnum++;
      }
    }
    /*
    vector<int> indices;
    
    for (int j = 0; j < CppMatrix.ncol(); j++) {
        int next = (j + 1) % CppMatrix.ncol();
        if (CppMatrix(i, j) == red && CppMatrix(i, next) == white)
          indices.push_back(j);
    }
    
    unsigned long curnum = Grid["currentNumOfRedMovement"];
    Grid["currentNumOfRedMovement"] = curnum + indices.size();

    for (vector<int>::iterator it = indices.begin(); it != indices.end(); ++it) {
        int j = *it;
        CppMatrix(i, j) = white;
        CppMatrix(i, (j + 1) % CppMatrix.ncol()) = red;
    }
    */
  }
  
  unsigned long cumnum = Grid["cumulativeNumOfRedMovement"];
  //unsigned long curnum = Grid["currentNumOfRedMovement"];
  Grid["cumulativeNumOfRedMovement"] = cumnum + curnum;
  Grid["currentNumOfRedMovement"] = curnum;
  
  return Grid;
}

// [[Rcpp::export(.moveBlueCpp)]]
List moveBlueCpp(List Grid) {
  SEXP Matrix = Grid["Matrix"];
  Rcpp::NumericMatrix CppMatrix = as<Rcpp::NumericMatrix>(Matrix);
  unsigned long curnum = 0;
  int n = CppMatrix.nrow();
  for (int j = 0; j < CppMatrix.ncol(); j++) {
    int start = n - 1;
    int end = 0;
    if (CppMatrix(end, j) == blue && CppMatrix(start, j) == white) {
      CppMatrix(end, j) = white;
      CppMatrix(start, j) = blue;
      start--;
      end++;
    }
    for (int i = start; i > end; i--) {
      if (CppMatrix(i, j) == blue && CppMatrix(i - 1, j) == white) {
        CppMatrix(i, j) = white;
        CppMatrix(i - 1, j) = blue;
        i--;
        curnum++;
      }
    }
    /*
    vector<int> indices;
    
    for (int i = CppMatrix.nrow() - 1; i >= 0; i--) {
      int prev = (i - 1 + CppMatrix.nrow()) % CppMatrix.nrow();
      if (CppMatrix(i, j) == blue && CppMatrix(prev, j) == white)
          indices.push_back(i);
    }
    
    unsigned long curnum = Grid["currentNumOfBlueMovement"];
    Grid["currentNumOfBlueMovement"] = curnum + indices.size();

    for (vector<int>::iterator it = indices.begin(); it != indices.end(); ++it) {
      int i = *it;
      CppMatrix(i, j) = white;
      CppMatrix((i - 1 + CppMatrix.nrow()) % CppMatrix.nrow(), j) = blue;
    }
    */
  }

  unsigned long cumnum = Grid["cumulativeNumOfBlueMovement"];
  //unsigned long curnum = Grid["currentNumOfBlueMovement"];
  Grid["cumulativeNumOfBlueMovement"] = cumnum + curnum;
  Grid["currentNumOfBlueMovement"] = curnum;
  return Grid;
}

// [[Rcpp::export(crunBMLGrid)]]
List crunBMLGrid(List BMLGrid, int numSteps) {
  BMLGrid["cumulativeNumOfRedMovement"] = 0.0;
  BMLGrid["cumulativeNumOfBlueMovement"] = 0.0;
  BMLGrid["currentNumOfRedMovement"] = 0.0;
  BMLGrid["currentNumOfBlueMovement"] = 0.0;
  BMLGrid["cumulativeRedSpeed"] = 0.0;
  BMLGrid["cumulativeBlueSpeed"] = 0.0;
  for (int i = 0; i < numSteps; i++) {
    BMLGrid = moveBlueCpp(BMLGrid);
    BMLGrid = moveRedCpp(BMLGrid);
    double x = BMLGrid["cumulativeNumOfRedMovement"];
    double xx = BMLGrid["numOfRed"];
    BMLGrid["cumulativeRedSpeed"] = x / i / xx;
    double y = BMLGrid["cumulativeNumOfBlueMovement"];
    double yy = BMLGrid["numOfBlue"];
    BMLGrid["cumulativeBlueSpeed"] = y / i / yy;
  }
  return BMLGrid;
}

// [[Rcpp::export(.copyGridCpp)]]
List copyGridCpp(List Grid) {
  return Rcpp::clone(Grid);
}
