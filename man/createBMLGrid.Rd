\name{createBMLGrid}
\alias{createBMLGrid}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Construct an S3 BMLGrid class. 
}
\description{
This function is a constructor of BML grid class. It constructs the class corresponding to the given number of rows, number of columns, number of red cars and numebr of blue cars. 
}
\usage{
createBMLGrid(r, c, ncars)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{r }{Number of rows in the grid.}

  \item{c }{Number of columns in the grid}

  \item{ncars}{A named vector, which must contain an elements named "red" and an element named "blue".}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
The BMLGrid S3 class, which contains:
\item{Matrix }{The layout of the cars. each cell with value -1 represents a red car, each cell with value 0 represents a empty cell, and value 1 represents a blue car.}
\item{cumulativeNumOfRedMovement }{This field records cumulative number of red car movements.}
\item{cumulativeNumOfBlueMovement }{This field records number of blue car movements.}
\item{currentNumOfRedMovement }{This field records the number of red car movements in the last step.}
\item{cumulativeBlueSpeed }{This field records the average speed of blue cars.}
\item{cumulativeRedSpeed }{This field records the average speed of red cars.}
\item{numOfRed }{The number of red cars.}
\item{numofBlue }{The number of blue cars.}
\item{useFunction }{This field records whether R movement method or native movement method is used.}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Xuesi Feng
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
library(FastBML)
g = createBMLGrid(10, 10, c(red = 15, blue = 15))
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
